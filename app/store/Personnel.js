Ext.define('pertemuan9.store.Personnel', {
    extend: 'Ext.data.Store',

    alias: 'store.personnel',
    storeId:'personnel',

    autoLoad: true,
    autoSync: true,

    fields: [
       'user_id','name', 'email', 'phone'
    ],

    proxy: {
        type: 'jsonp',
        api:{
              read   : "http://localhost/MyApp_php/readpersonnel.php",
              update : "http://localhost/MyApp_php/updatePersonnel.php",
              destroy: "http://localhost/MyApp_php/destroyPersonnel.php",
              create : "http://localhost/MyApp_php/createPersonnel.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    },
    Listeners:{
        beforeload:function(store, operation, eOpts){
            this.getProxy().setExtraParams({
                user_id: -1
            });
        }
    }
});
